from flask import Flask
from flask import request
from flask import jsonify
from errors import *
from uuid import uuid4
import neural_api as n_api
import data_processing as dp
import os


app = Flask(__name__)
ROOT = os.path.abspath(os.path.dirname(__file__)) + "/nets"


# App router to requests
@app.route('/ping')
def hello_world():
    return 'pong pong...'


# Request for creating new net, and save it in file.
# Return net_id
@app.route('/weave')
def create_new_net():
    try:
        input_dim = int(request.args.get('max_num_of_answers')) * int(request.args.get('q_number'))
        class_num = int(request.args.get('num_of_final_options'))
    except ValueError or TypeError:
        raise NetError("Args must be integer type!", status_code=400)

    model = n_api.create_neural_net(input_dim, class_num)
    net_id = str(uuid4())
    n_api.save_neural_net(model, ROOT, net_id)

    del model

    return jsonify(net_id)


# Request for teach existing web, and save renew its file.
# Return net_id
@app.route('/web_interlacing')
def fit_existing_net():
    try:
        net_id = str(request.args.get('net_id'))
        train_data_x = str(request.args.get('q_answers'))
        train_data_y = str(request.args.get('expected_result'))
    except TypeError:
        raise NetError("WTF are you doing man?!", status_code=400)

    if not dp.validate_input_data(train_data_x):
        raise NetError("Wrong format of q_answers parameter!", status_code=400)
    elif not dp.validate_input_data(train_data_y):
        raise NetError("Wrong format of expected_result parameter!", status_code=400)
    else:
        train_data_x = dp.parse_data(train_data_x)
        train_data_y = dp.parse_data(train_data_y)

    try:
        model = n_api.load_neural_net(ROOT, net_id)
    except FileNotFoundError:
        raise NetError("No network uses such an identifier!", status_code=400)

    n_api.make_fit(model, train_data_x, train_data_y)
    n_api.save_neural_net(model, ROOT, net_id)

    del model

    return jsonify(net_id)


# Request for predict values with neural net.
# Return result of predicting.
@app.route('/crawl')
def predict_with_existing_net():
    try:
        net_id = str(request.args.get('net_id'))
        data_x = str(request.args.get('q_answers'))
    except TypeError:
        raise NetError("WTF are you doing man?!", status_code=400)

    if not dp.validate_input_data(data_x):
        raise NetError("Wrong format of q_answers parameter!", status_code=400)
    else:
        data_x = dp.parse_data(data_x)

    try:
        model = n_api.load_neural_net(ROOT, net_id)
    except FileNotFoundError:
        raise NetError("No network uses such an identifier!", status_code=400)

    clean_result, result = n_api.get_predict(model, data_x)
    del model

    return jsonify(clean_result)

# Error handlers
@app.errorhandler(NetError)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# if __name__ == '__main__':
#     app.run()
if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
