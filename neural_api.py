from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD
from keras.models import load_model
import numpy as np
import os

# Creating new Keras model
# With len of input = input_dim
# And number of classes = class_dim
def create_neural_net(input_dim, class_dim):
    model = Sequential()
    model.add(Dense(256, activation='relu', input_dim=input_dim))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(class_dim, activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy',
                  optimizer=sgd,
                  metrics=['accuracy'])

    return model


# Save Keras model to file in nets dir
def save_neural_net(model, path, filename):
    try:
        os.remove(path + "/" + filename + '.h5')
    except OSError:
        pass

    model.save(path + "/" + filename + '.h5')

    return filename + '.h5'


# Load Keras model to file in nets dir
def load_neural_net(path, filename):
    model = load_model(path + "/" + filename + '.h5')

    return model


# Fit model
def make_fit(model, x_train, y_train):
    model.fit(np.array(x_train), np.array(y_train),
              epochs=20,
              batch_size=128)


# Make predict for some input
def get_predict(model, x_predict):
    result = model.predict(np.array(x_predict))
    clean_result = list()
    for i in result:
        clean_result.append(i.index(max(i)))

    return clean_result, result
