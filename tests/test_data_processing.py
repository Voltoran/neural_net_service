import unittest
import data_processing as dp
import numpy as np


class TestDataProcessing(unittest.TestCase):
    def setUp(self):
        self.parse_input = ["[[0, 0, 1], [1, 0, 0], [0, 1, 0]]",
                            "[[0,1, [1, 0, 0], [0, 1, 0]]",
                            "[[0, 0, 1;, [quwhe, 0fqw,qwd qd], [0qwd, 1, 0]]",
                            "",
                            None]

        self.parse_expected_output = np.asarray([[0, 0, 1, 1, 0, 0, 0, 1, 0],
                                                 [0, 1, 1, 0, 0, 0, 1, 0],
                                                 None,
                                                 None,
                                                 None])

        self.validate_input = ["[[0, 0, 1], [1, 0, 0], [0, 1, 0]]",
                               "[[0,0,1],[1,0, 0],[0, 1, 0]]",
                               "[[0, 1], [0], [0, 0, 1, 0]]",

                               "[[0,1, [1, 0, 0], [0, 1, 0]]",
                               "[[0,1, [1, 0, 0] [0, 1, 0]]",

                               "[[0, 0, 1;, [quwhe, 0fqw,qwd qd], [0qwd, 1, 0]]",
                               "[1, 0, 1, 0, 0]",
                               "[[0, 3, 1], [1, 0, 5], [2345678, 1, 0]]",
                               "",
                               None]
        self.validate_expected_output = np.asarray([True, True, True,
                                                    False, False, False, False, False, False, False])

    def test_validate_1(self):
        for i in range(3):
            np.testing.assert_array_equal(dp.validate_input_data(self.validate_input[i]),
                                          self.validate_expected_output[i])

    def test_validate_2(self):
        for i in range(3, 6):
            np.testing.assert_array_equal(dp.validate_input_data(self.validate_input[i]),
                                          self.validate_expected_output[i])

    def test_validate_3(self):
        for i in range(6, 8):
            np.testing.assert_array_equal(dp.validate_input_data(self.validate_input[i]),
                                          self.validate_expected_output[i])

    def test_parse_1(self):
        for i in range(0):
            np.testing.assert_array_equal(dp.parse_data(self.parse_input[i]),
                                          self.parse_expected_output[i])

    def test_parse_2(self):
        for i in range(1):
            np.testing.assert_array_equal(dp.parse_data(self.parse_input[i]),
                                          self.parse_expected_output[i])

    def test_parse_3(self):
        for i in range(2):
            np.testing.assert_array_equal(dp.parse_data(self.parse_input[i]),
                                          self.parse_expected_output[i])

if __name__ == '__main__':
    unittest.main()
