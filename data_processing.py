import numpy as np
import re


# Validate input data
# Data must be like this -> string: "[[1, 0, 0, 0], [1, 1, 0, 0], [0, 0 ,0 , 1]]"
# Every subarray is question answer set
def validate_input_data(data):
    if data is not None or "":
        data = data.replace(" ", "").replace("\t", "")
    else:
        return False

    if re.match(r'\[{1}((\[[0,1])(,[0,1])*(\]))(,(\[[0,1])(,[0,1])*\])*(\]{1})$', data):
        return True
    else:
        return False


# Parse valid data, if got wrong input return NONE
def parse_data(data):
    if data is not None:
        data = data.replace("[", "").replace("]", "").replace(" ", "").split(',')
    else:
        return None

    try:
        result = np.asarray(list(map(lambda x: int(x), data)))
    except ValueError:
        return None

    return result
